#!/bin/bash

# Aurrebaldintza: $1 - Datubasea ; $2 - weka.jar kokapena
#                 $2 ez bada idazten, aurre definitutako bat erabiliko da
#                 $1 idazten ez bada, uneko karpetan exekutatuko da.
# Eragina: Datubasean prozesatu.sh aplikatzen du.
#          Convert aginduak egiten ditu originalak karpetaren gainean beste 5 transformazio lortzeko
#          Karpeta bakoitzarentzat imageFilter paketeko 3 filter aplikatzen dira
#          12 classify egingo dira imageFilterrekin lortutako emaitza bakoitzarekin.
#          Emaitza guztiak LaTeX formatuko taula batzuean inprimatuko dira.

if [ "$1" = "" ]
then
	dir=$(pwd)
else
	if [[ ! -e "$(readlink -f $1)" ]]; then
		echo "$1 ez da existitzen"
		exit 1
	fi
	if [[ ! -d "$(readlink -f $1)" ]]; then
		echo "$1 ez da direktorio bat"
		exit 2 
	fi

	dir="${1%/*}"
fi


if [ "$2" = "" ]
then
	classpath=/usr/share/java/weka/weka.jar
else
	if [[ ! -e "$(readlink -f $2)" ]]; then
		echo "$2 ez da existitzen"
		exit 3
	fi
	if [[ ! -f "$(readlink -f $2)" ]]; then
		echo "$2 ez da fitxategi bat"
		exit 4 
	fi
	echo 4

	classpath="$2"
fi

wekafolder="$dir/weka/"
zerrenda="$wekafolder""zerrenda.arff"
originalsfolder="$wekafolder""originalak"

bash prozesatu.sh $dir # Non dago prozesatu.sh?

subdir=$(dirname $dir)

# zeintzuk izango dira convert agindu motak
declare -A eraldaketa
eraldaketa[1]="edge"
eraldaketa[2]="gaussian-blur"
eraldaketa[3]="gamma"
eraldaketa[4]="motion-blur"
eraldaketa[5]="sketch"

# imageFIlter motak
prepro1="EdgeHistogramFilter"
prepro2="SimpleColorHistogramFilter"
prepro3="ColorLayoutFilter"

mkdir "$wekafolder"${eraldaketa[1]} 2> /dev/null
mkdir "$wekafolder"${eraldaketa[2]} 2> /dev/null
mkdir "$wekafolder"${eraldaketa[3]} 2> /dev/null
mkdir "$wekafolder"${eraldaketa[4]} 2> /dev/null
mkdir "$wekafolder"${eraldaketa[5]} 2> /dev/null

num=0

# Convert eginduak egiten ditu
for file in `find $originalsfolder -type f`
do
#	width=$(identify -format "%w" "$file")> /dev/null
#	height=$(identify -format "%h" "$file")> /dev/null
	extension="${file##*.}" 
	if [[ "$extension" = "arff" || "$extension" = "txt" ]]; then
		continue
	fi
	echo "$num. irudia:"
	num=$((num + 1))
    fname=$(basename "$file")
    for D in $wekafolder*/
    do
    	basedir=$(basename "$D")
    	if [ "$basedir" = "originalak" ]; then
			continue
		fi
		if [ -f "$D$fname" ]; then
			echo "$D$fname already exists"
			continue
		else
			# Parametroen aldaketa behar liteke eraldaketa aldatuz gero
			echo "$fname""==""$D"	
			if [ "$basedir" = "${eraldaketa[1]}" ]; then
				echo "${eraldaketa[1]}"
				convert "$file" -"${eraldaketa[1]}" 2 "$D""$fname"
			fi
			if [ "$basedir" = "${eraldaketa[2]}" ]; then
				echo "${eraldaketa[2]}"
				convert "$file" -"${eraldaketa[2]}" 3x3 $D$fname
			fi
			if [ "$basedir" = "${eraldaketa[3]}" ]; then
				echo "${eraldaketa[3]}"
				convert "$file" -"${eraldaketa[3]}" 5 $D$fname
			fi
			if [ "$basedir" = "${eraldaketa[4]}" ]; then
				echo "${eraldaketa[4]}"
				convert "$file" -"${eraldaketa[4]}" 3x3 $D$fname
			fi
			if [ "$basedir" = "${eraldaketa[5]}" ]; then
				echo "${eraldaketa[5]}"
				convert "$file" -"${eraldaketa[5]}" 3x3 $D$fname
			fi
		fi
    done
done

export CLASSPATH="$classpath"


# Aurre prozesaketa egin
for D in $wekafolder*/
do
	echo "$D"
	if [ ! -f "$D"$prepro1.arff ]; then
		echo "$prepro1"
		java weka.Run weka.filters.unsupervised.instance.imagefilter.$prepro1 -D "$D" -i "$zerrenda" -o "$D"$prepro1.arff
		sed -i -E -r 's/@attribute Izena string//g' "$D"$prepro1.arff 
		sed -i -E -r 's/^.*\.((jpeg)|(jpg)|(png)),//g' "$D"$prepro1.arff
		echo "done"
	fi

	if [ ! -f "$D"$prepro2.arff ]; then
		echo "$prepro2"
		java weka.Run weka.filters.unsupervised.instance.imagefilter.$prepro2 -D "$D" -i "$zerrenda" -o "$D"$prepro2.arff
		sed -i -E -r 's/@attribute Izena string//g' "$D"$prepro2.arff 
		sed -i -E -r 's/^.*\.((jpeg)|(jpg)|(png)),//g' "$D"$prepro2.arff
		echo "done"
	fi

	if [ ! -f "$D"$prepro3.arff ]; then
		echo "$prepro3"
		java weka.Run weka.filters.unsupervised.instance.imagefilter.$prepro3 -D "$D" -i "$zerrenda" -o "$D"$prepro3.arff
		sed -i -E -r 's/@attribute Izena string//g' "$D"$prepro3.arff 
		sed -i -E -r 's/^.*\.((jpeg)|(jpg)|(png)),//g' "$D"$prepro3.arff
		echo "done"
	fi
done

# Classify
for D in $wekafolder*/
do
	echo $D
	for file in `find $D -type f | grep .arff`
	do
		file_no_path=$(basename "$file")
		echo "$file_no_path"
		file_no_ext="${file_no_path%.*}"
		if [ ! -f "$D"IBk1-"$file_no_ext".txt ]; then
			java -Xmx2048M weka.classifiers.lazy.IBk -K 1 -W 0 -I -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.EuclideanDistance -R first-last\"" -t $file -x 10 > "$D"IBk1-"$file_no_ext".txt 
		echo "IBk1 done"
		fi
		if [ ! -f "$D"IBk3-"$file_no_ext".txt ]; then
			java -Xmx2048M weka.classifiers.lazy.IBk -K 3 -W 0 -I -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.EuclideanDistance -R first-last\"" -t $file -x 10 > "$D"IBk3-"$file_no_ext".txt 
			echo "IBk3 done"
		fi
		if [ ! -f "$D"IBk5-"$file_no_ext".txt ]; then
			java -Xmx2048M weka.classifiers.lazy.IBk -K 5 -W 0 -I -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.EuclideanDistance -R first-last\"" -t $file -x 10 > "$D"IBk5-"$file_no_ext".txt 
			echo "IBk5 done"
		fi
		if [ ! -f "$D"IBk9-"$file_no_ext".txt ]; then
			java -Xmx2048M weka.classifiers.lazy.IBk -K 9 -W 0 -I -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.EuclideanDistance -R first-last\"" -t $file -x 10 > "$D"IBk9-"$file_no_ext".txt 
			echo "IBk9 done"
		fi
		if [ ! -f "$D"IBk1_noWeidth-"$file_no_ext".txt ]; then
			java -Xmx2048M weka.classifiers.lazy.IBk -K 1 -W 0 -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.EuclideanDistance -R first-last\"" -t $file -x 10 > "$D"IBk1_noWeidth-"$file_no_ext".txt 
		echo "IBk1_noWeidth done"
		fi
		if [ ! -f "$D"IBk3_noWeidth-"$file_no_ext".txt ]; then
			java -Xmx2048M weka.classifiers.lazy.IBk -K 3 -W 0 -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.EuclideanDistance -R first-last\"" -t $file -x 10 > "$D"IBk3_noWeidth-"$file_no_ext".txt 
			echo "IBk3_noWeidth done"
		fi
		if [ ! -f "$D"IBk5_noWeidth-"$file_no_ext".txt ]; then
			java weka.classifiers.lazy.IBk -K 5 -W 0 -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.EuclideanDistance -R first-last\"" -t $file -x 10 > "$D"IBk5_noWeidth-"$file_no_ext".txt 
			echo "IBk5_noWeidth done"
		fi
		if [ ! -f "$D"IBk9_noWeidth-"$file_no_ext".txt ]; then
			java -Xmx2048M weka.classifiers.lazy.IBk -K 9 -W 0 -A "weka.core.neighboursearch.LinearNNSearch -A \"weka.core.EuclideanDistance -R first-last\"" -t $file -x 10 > "$D"IBk9_noWeidth-"$file_no_ext".txt 
			echo "IBk9_noWeidth done"
		fi
		if [ ! -f "$D"J48-"$file_no_ext".txt ]; then
			java -Xmx2048M weka.classifiers.trees.J48 -C 0.25 -M 2 -t $file -x 10 > "$D"J48-"$file_no_ext".txt 
			echo "J48 done"
		fi
		if [ ! -f "$D"NaiveBayes-"$file_no_ext".txt ]; then
			java -Xmx2048M weka.classifiers.bayes.NaiveBayes -t $file -x 10 > "$D"NaiveBayes-"$file_no_ext".txt 
			echo "Naive done"
		fi
		if [ ! -f "$D"KStar-"$file_no_ext".txt ]; then
			java -Xmx2048M weka.classifiers.lazy.KStar -B 20 -M a -t $file -x 10 > "$D"KStar-"$file_no_ext".txt 
			echo "KStar done"
		fi
		if [ ! -f "$D"HoeffdingTree-"$file_no_ext".txt ]; then
			java -Xmx2048M weka.classifiers.trees.HoeffdingTree -L 2 -S 1 -E 1.0E-7 -H 0.05 -M 0.01 -G 200.0 -N 0.0 -t $file -x 10 > "$D"HoeffdingTree-"$file_no_ext".txt
			echo "HoeffdingTree done"
		fi
	done
done


# Taulak kalkulatu
 bash table.sh "$prepro1" "$wekafolder" > "$wekafolder"edgeTable.txt
 bash table.sh "$prepro2" "$wekafolder" > "$wekafolder"simpleTable.txt
 bash table.sh "$prepro3" "$wekafolder" > "$wekafolder"colorTable.txt


