#!/bin/bash

# Aurrebaldintza: $1 - Aurre-prozesaketa izena ; $2 - Datubasean sortutako weka karpeta 
#                 $2 idazten ez bada, uneko karpetan exekutatuko da.
# Eragina: Emandako aurre-prozesaketarekin lortu diren fitxategiak bilatzen ditu eta
#          horiekin lortu diren emaitzak taula batean inprimatzen ditu.

if [ "$2" = "" ]
then
	dir=$(pwd)
else
	if [[ ! -e "$(readlink -f $2)" ]]; then
		echo "$2 ez da existitzen"
		exit 1
	fi
	if [[ ! -d "$(readlink -f $2)" ]]; then
		echo "$2 ez da direktorio bat"
		exit 2 
	fi

	dir="${2%/*}"
fi


# Puntuak erabili "coma" higikorreko aldagaietan "puntu" ordez
export LC_NUMERIC="en_US.UTF-8"

# Fitxategi batean interesatzen den probabilitatea itzultzen du
get_num(){

	NUM=$( grep -n "=== Stratified cross-validation ===" $1 | cut -d : -f 1 )
	# echo $NUM
	NUM1=$((NUM + 2))
	# echo $NUM1
	line=$( sed "${NUM1}q;d" $1 )
	# echo $line
	number=$( echo $line | grep -Eo '[+-]?[0-9]+([.][0-9]+)?' | grep [*.*] )

	echo $number

}


# Create matrix
declare -A matrix
num_rows=12
num_columns=6

# for ((i=1;i<=num_rows;i++)) do
#     for ((j=1;j<=num_columns;j++)) do
#         matrix[$i,$j]=$RANDOM
#     done
# done

f2=" %5.3f\n"
i=1

# General mean batezbestekoen batezbestekoa da
general_mean=0
# BIlatu direktorio guztietan
for D in `find $dir -type d`
do
	if [ "$D" = "$dir" ]; then
		continue
	fi

	# echo $D
	j=1
	mean=0
	# .txt diren fitxategiak. Hauek baitute emaitzen informazioa
	for file in $D/*-$1.txt
	do
		# echo "$j = $(basename $file)"
		number=$( get_num $file )
		mean=$( echo $mean + $number | bc )

		matrix[$j,$i]=$number

		j=$(($j + 1))
	done

	mean=$( echo $mean/$num_rows | bc -l )	
	general_mean=$( echo $general_mean + $mean | bc )
	matrix[$j,$i]=$(printf $f2 $mean)

	i=$(($i + 1))
done

general_mean=$( echo $general_mean/$num_columns | bc -l )	
matrix[$(( $num_rows + 1 )),$(( $num_columns + 1 ))]=$(printf $f2 $general_mean)

# Errenkaden batezbestekoa kalkulatzeko
for ((row=1;row<=num_rows;row++)) do
	mean=0
	for ((i=1;i<=num_columns;i++)) do
		mean=$( echo $mean + ${matrix[$row,$i]} | bc )
	done
	mean=$( echo $mean/$num_columns | bc -l )	
	matrix[$row,$(( $num_columns + 1 ))]=$(printf $f2 $mean)
done


echo "		\\begin{table}[H]"
echo "		\\centering"
echo "		\\begin{tabular}{|l|l|l|l|l|l|l|l|}"
echo "		\\hline"
echo "		{\\ul \\textbf{Asmatze Tasak}} & Edge & Gamma & GaussianB & MotionB & Originalak & Sketch & \\textbf{Iragaz.} \\\\ \\hline"
echo "		1-NN & ${matrix[3,1]} & ${matrix[3,2]} & ${matrix[3,3]} & ${matrix[3,3]} & ${matrix[3,5]} & ${matrix[3,6]} & ${matrix[3,7]} \\\\ \\hline"
echo "		1-NN P & ${matrix[2,1]} & ${matrix[2,2]} & ${matrix[2,3]} & ${matrix[2,2]} & ${matrix[2,5]} & ${matrix[2,6]} & ${matrix[2,7]} \\\\ \\hline"
echo "		3-NN & ${matrix[5,1]} & ${matrix[5,2]} & ${matrix[5,3]} & ${matrix[5,4]} & ${matrix[5,5]} & ${matrix[5,6]} & ${matrix[5,7]} \\\\ \\hline"
echo "		3-NN P & ${matrix[4,1]} & ${matrix[4,2]} & ${matrix[4,3]} & ${matrix[4,4]} & ${matrix[4,5]} & ${matrix[4,6]} & ${matrix[4,7]} \\\\ \\hline"
echo "		5-NN & ${matrix[7,1]} & ${matrix[7,2]} & ${matrix[7,3]} & ${matrix[7,4]}  & ${matrix[7,5]} & ${matrix[7,6]} & ${matrix[7,7]} \\\\ \\hline"
echo "		5-NN P & ${matrix[6,1]} & ${matrix[6,2]} & ${matrix[6,3]} & ${matrix[6,4]} & ${matrix[6,5]} & ${matrix[6,6]} & ${matrix[6,7]} \\\\ \\hline"
echo "		9-NN & ${matrix[9,1]} & ${matrix[9,2]} & ${matrix[9,3]} & ${matrix[9,4]} & ${matrix[9,5]} & ${matrix[9,6]} & ${matrix[9,7]} \\\\ \\hline"
echo "		9-NN P & ${matrix[8,1]} & ${matrix[8,2]} & ${matrix[8,3]} & ${matrix[8,4]} & ${matrix[8,5]} & ${matrix[8,6]} & ${matrix[8,7]} \\\\ \\hline"
echo "		C4.5 & ${matrix[10,1]} & ${matrix[10,2]} & ${matrix[10,3]} & ${matrix[10,4]} & ${matrix[10,5]} & ${matrix[10,6]} & ${matrix[10,7]} \\\\ \\hline"
echo "		Naive Bayes & ${matrix[12,1]} & ${matrix[12,2]} & ${matrix[12,3]} & ${matrix[12,4]} & ${matrix[12,5]} & ${matrix[12,6]} & ${matrix[12,7]} \\\\ \\hline"
echo "		KStar & ${matrix[11,1]} & ${matrix[11,2]} & ${matrix[11,3]} & ${matrix[11,4]} & ${matrix[11,5]} & ${matrix[11,6]} & ${matrix[11,7]} \\\\ \\hline"
echo "		HoeffdingTree & ${matrix[1,1]} & ${matrix[1,2]} & ${matrix[1,3]} & ${matrix[1,4]} & ${matrix[1,5]} & ${matrix[1,6]} & ${matrix[1,7]} \\\\ \\hline"
echo "		\\textbf{Convert BB} & ${matrix[13,1]} & ${matrix[13,2]} & ${matrix[13,3]} & ${matrix[13,4]} & ${matrix[13,5]} & ${matrix[13,6]} & \\textbf{${matrix[13,7]}} \\\\ \\hline"
echo "		\\end{tabular}"
echo "		\\end{table}"
