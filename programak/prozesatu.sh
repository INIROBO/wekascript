#!/bin/bash

# Aurrebaldintza: $1 - Datubasea
#                 $1 idazten ez bada, uneko karpetan exekutatuko da.
#                 ImageMagick-ko convert agindua eskuragarri dauka egon beharra.
# Emaitza: Emandako datubasean dauden karpeta guztietako fitxategi guztiak
#          datubaseko beste karpeta berri batean kopiatzen ditu eta
#          fitxategiekin .arff motako zerrenda bat sortzen du karpeta honetan.
#          Fitxategiak $DatuBasea/weka/originalak/ karpetan egongo dira.


if [ "$1" = "" ]
then
	dir=$(pwd)
else
	if [[ ! -e "$(readlink -f $1)" ]]; then
		echo "$1 ez da existitzen"
		exit 1
	fi
	if [[ ! -d "$(readlink -f $1)" ]]; then
		echo "$1 ez da direktorio bat"
		exit 2 
	fi

	dir="${1%/*}"
fi

wekafolder="$dir/weka/"
zerrenda="$wekafolder""zerrenda.arff"
exists=false

# Hasieratuta dago
if [ -f $zerrenda ]
then
	echo "First arff EXISTS"
	exists=true
fi

# Hasieratu
if [ "$exists" = false ]; then
	mkdir $wekafolder
	mkdir $wekafolder"originalak/"
	touch $zerrenda

	# Setup arff file
	echo "@relation DM" >> $zerrenda
	echo "@attribute Izena string" >> $zerrenda

	# Klaseak lortu eta aldagai batean sartu komekin
	klaseak=""
	coma=""
	for D in $dir/*/
	do
		if [ "$(basename $D)" != "$(basename $wekafolder)" ]; then
			feature=$(basename $D)
			klaseak="$klaseak$coma$feature"
			coma=","
		fi
	done
	klaseak="$klaseak$coma"
	klaseak=${klaseak::-1} # azken koma kentzeko

	echo "@attribute Klasea {$klaseak}" >> $zerrenda

	echo "" >> $zerrenda
	echo "@data" >> $zerrenda
fi


# Kopiatu fitxategiak
for D in $dir/*/
do
    feature=$(basename $D)
	wekabase=$(basename $wekafolder)
	if [ "$feature" = "$wekabase" ]; then
		continue
	else
		for file in `find $D -type f`
		do
            fname=$(basename "$file")
			if [[ "$exists" = true ]]; then
				# Dagoeneko badago zerrendan ez egin zer
				if ! grep -Fxq "$feature""_""$fname"",""$feature" "$zerrenda"
				then
					echo $file
					cp -n $file $wekafolder"/originalak/"$feature"_"$fname
					echo $feature"_"$fname,$feature >> $zerrenda
				fi
			else
				echo $file
				cp -n $file $wekafolder"/originalak/"$feature"_"$fname
				echo $feature"_"$fname,$feature >> $zerrenda
			fi
		done
	fi
done

